import request from '@/utils/request'

export function getChainData(hash) {
  return request({
    url: `/blockchain/confession/qixi/${hash}`,
    method: 'get'
  })
}
