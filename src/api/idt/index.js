import request from '@/utils/request'

export function upload(content) {
  return request({
    url: '/blockchain/confession/qixi ',
    method: 'post',
    data: {
      content,
      corporation: 2
    }
  })
}

export function getInfo(hash) {
  return request({
    url: `/blockchain/confession/qixi/${hash}`,
    method: 'get'
  })
}

