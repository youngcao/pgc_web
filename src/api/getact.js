import request from '@/utils/request'
export function ConfessionChain(data) {
  console.log(data)
  return request({
    url: 'blockchain/confession/qixi',
    method: 'post',
    data
  })
}
